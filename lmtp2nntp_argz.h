/*
**  OSSP lmtp2nntp - Mail to News Gateway
**  Copyright (c) 2001-2003 Ralf S. Engelschall <rse@engelschall.com>
**  Copyright (c) 2001-2003 The OSSP Project <http://www.ossp.org/>
**  Copyright (c) 2001-2003 Cable & Wireless Germany <http://www.cw.com/de/>
**
**  This file is part of OSSP lmtp2nntp, an LMTP speaking local
**  mailer which forwards mails as Usenet news articles via NNTP.
**  It can be found at http://www.ossp.org/pkg/tool/lmtp2nntp/.
**
**  This program is free software; you can redistribute it and/or
**  modify it under the terms of the GNU General Public  License
**  as published by the Free Software Foundation; either version
**  2.0 of the License, or (at your option) any later version.
**
**  This program is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
**  General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with this file; if not, write to the Free Software
**  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
**  USA, or contact the OSSP project <ossp@ossp.org>.
**
**  lmtp2nntp_argz.c: Zero-Terminated Argument Vector library (API)
*/

/* Routines for dealing with '\0' separated arg vectors.
   Copyright (C) 1996, 1997 Free Software Foundation, Inc.
   This file is part of the GNU C Library.
   Contributed by Ulrich Drepper <drepper@gnu.ai.mit.edu>, 1996.

   The GNU C Library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Library General Public License as
   published by the Free Software Foundation; either version 2 of the
   License, or (at your option) any later version.

   The GNU C Library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public
   License along with the GNU C Library; see the file COPYING.LIB.  If not,
   write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
   Boston, MA 02111-1307, USA.  */

#ifndef _ARGZ_H_
#define _ARGZ_H_

#include <string.h>

extern int    argz_create    (char *const _argv[], char **_argz, size_t * _len);
extern int    argz_create_sep(const char *_string, int _sep, char **_argz, size_t * _len);
extern size_t argz_count     (const char *_argz, size_t _len);
extern void   argz_extract   (const char *_argz, size_t _len, char **_argv);
extern void   argz_stringify (char *_argz, size_t _len, int _sep);
extern int    argz_append    (char **_argz, size_t * _argz_len, const char *_buf, size_t _buf_len);
extern int    argz_add       (char **_argz, size_t * _argz_len, const char *_str);
extern int    argz_add_sep   (char **_argz, size_t * _argz_len, const char *_string, int _delim);
extern void   argz_delete    (char **_argz, size_t * _argz_len, char *_entry);
extern int    argz_insert    (char **_argz, size_t * _argz_len, char *_before, const char *_entry);
extern int    argz_replace   (char **_argz, size_t * _argz_len, const char *_str, const char *_with, unsigned int *_replace_count);
extern char  *argz_next      (const char *_argz, size_t _argz_len, const char *_entry);

#endif /* _ARGZ_H_ */

