/*
**  OSSP lmtp2nntp - Mail to News Gateway
**  Copyright (c) 2002-2003 Ralf S. Engelschall <rse@engelschall.com>
**  Copyright (c) 2002-2003 The OSSP Project <http://www.ossp.org/>
**  Copyright (c) 2002-2003 Cable & Wireless Germany <http://www.cw.com/de/>
**
**  This file is part of OSSP lmtp2nntp, an LMTP speaking local
**  mailer which forwards mails as Usenet news articles via NNTP.
**  It can be found at http://www.ossp.org/pkg/tool/lmtp2nntp/.
**
**  This program is free software; you can redistribute it and/or
**  modify it under the terms of the GNU General Public  License
**  as published by the Free Software Foundation; either version
**  2.0 of the License, or (at your option) any later version.
**
**  This program is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
**  General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with this file; if not, write to the Free Software
**  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
**  USA, or contact the OSSP project <ossp@ossp.org>.
**
**  lmtp2nntp_exwrap.c: lib_ex wrappers
*/

#include <string.h>
#include <stdlib.h>
#ifdef HAVE_CONFIG_H
#include "config.h"
#endif
#if defined(HAVE_DMALLOC_H) && defined(WITH_DMALLOC)
#include "dmalloc.h"
#endif

/* own headers */
#include "lmtp2nntp_global.h"

char *strdupex(const char *str)
{
    char *rc;

    if (str == NULL)
        throw(0,0,0);
    if ((rc = strdup(str)) == NULL)
        throw(0,0,0);
    return rc;
}

char *str_dupex(const char *s, str_size_t n)
{
    char *rc;

    if (s == NULL || n == 0)
        throw(0,0,0);
    if ((rc = str_dup(s, n)) == NULL)
        throw(0,0,0);
    return rc;
}

void *mallocex(size_t size)
{
    void *rc;

    if (size == 0)
        throw(0,0,0);
    if ((rc = malloc(size)) == NULL)
        throw(0,0,0);
    return rc;
}

void *reallocex(void *ptr, size_t size)
{
    void *rc;

    if (ptr == NULL || size == 0)
        throw(0,0,0);
    if ((rc = realloc(ptr, size)) == NULL)
        throw(0,0,0);
    return rc;
}

void freeex(void *ptr)
{
    if (ptr == NULL)
        throw(0,0,0);
    free(ptr);
    return;
}

