/*
**  OSSP lmtp2nntp - Mail to News Gateway
**  Copyright (c) 2002-2003 Ralf S. Engelschall <rse@engelschall.com>
**  Copyright (c) 2002-2003 The OSSP Project <http://www.ossp.org/>
**  Copyright (c) 2002-2003 Cable & Wireless Germany <http://www.cw.com/de/>
**
**  This file is part of OSSP lmtp2nntp, an LMTP speaking local
**  mailer which forwards mails as Usenet news articles via NNTP.
**  It can be found at http://www.ossp.org/pkg/tool/lmtp2nntp/.
**
**  This program is free software; you can redistribute it and/or
**  modify it under the terms of the GNU General Public  License
**  as published by the Free Software Foundation; either version
**  2.0 of the License, or (at your option) any later version.
**
**  This program is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
**  General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with this file; if not, write to the Free Software
**  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
**  USA, or contact the OSSP project <ossp@ossp.org>.
**
**  lmtp2nntp_common.h: common stuff
*/

#ifndef __LMTP2NNTP_COMMON_H__
#define __LMTP2NNTP_COMMON_H__

#include <val.h>
#include <var.h>
#include <sa.h>

struct acl {
    char      *acl;
    int        not;
    size_t     prefixlen;
    sa_addr_t *saa;
};

#include "lmtp2nntp_nntp.h"
struct ns {
    sa_addr_t      *saa;
    sa_t           *sa;
    nntp_t         *nntp;
    nntp_rc_t       rc;
    l2_channel_t   *l2;
};

struct session {
    int     lhlo_seen;
    char   *lhlo_domain;
};

#include <sys/utsname.h>
typedef struct {
    l2_context_t    ctx;
    val_t          *prival;
    val_t          *val;
    char           *progname;
    int             option_groupmode;
    int             option_operationmode;
    char           *option_operationmodefakestatus;
    char           *option_operationmodefakedsn;
    int             option_maxmessagesize;
    headerrule_t   *option_firstheaderrule;
    int             option_timeout_lmtp_accept;
    int             option_timeout_lmtp_read;
    int             option_timeout_lmtp_write;
    int             option_timeout_nntp_connect;
    int             option_timeout_nntp_read;
    int             option_timeout_nntp_write;
    char           *option_nodename;
    char           *option_mailfrom;
    char           *option_restrictheader;
    char           *option_pidfile;
    int             option_killflag;
    uid_t           option_uid;
    int             option_daemon;
    int             nacl; /* number of acl structures found at pacl */
    struct acl     *pacl; /* pointer to an array of acl structures */
    int             option_childsmax;
    int             active_childs;
    l2_env_t       *l2_env;
    l2_channel_t   *l2;
    sa_addr_t      *saaServerbind;
    sa_t           *saServerbind;
    sa_addr_t      *saaClientbind;
    sa_t           *saClientbind;
    sa_addr_t      *saaIO;
    sa_t           *saIO;
    int             fdIOi;
    int             fdIOo;
    int             nns; /* number of ns structures found at pns */
    struct ns      *pns; /* pointer to an array of ns structures */
    char           *azGroupargs;
    size_t          asGroupargs;
    struct          session session;
    msg_t          *msg;
    var_t          *config_varregex;
    var_t          *config_varctx;
    int             msgcount; /* number of messages processed, used for creating unique ids */
} lmtp2nntp_t;

#define ERR_EXECUTION 1
#define ERR_DELIVERY -2

enum {
    GROUPMODE_UNDEF = 0,
    GROUPMODE_ARG,
    GROUPMODE_ENVELOPE,
    GROUPMODE_HEADER
};

enum {
    OPERATIONMODE_UNDEF = 0,
    OPERATIONMODE_FAKE,
    OPERATIONMODE_POST,
    OPERATIONMODE_FEED
};

void msg_headermatrixbuildup(msg_t *msg);
void msg_headermatrixteardwn(msg_t *msg);
void headerrewrite(lmtp2nntp_t *ctx);

#include "l2.h"
extern l2_handler_t l2_handler_var;
extern void logbook(l2_channel_t *, l2_level_t, const char *, ...);

#endif /* __LMTP2NNTP_COMMON_H__ */
