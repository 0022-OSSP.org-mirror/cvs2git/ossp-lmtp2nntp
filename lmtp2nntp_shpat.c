/*
**  OSSP lmtp2nntp - Mail to News Gateway
**  Copyright (c) 2001-2003 Ralf S. Engelschall <rse@engelschall.com>
**  Copyright (c) 2001-2003 The OSSP Project <http://www.ossp.org/>
**  Copyright (c) 2001-2003 Cable & Wireless Germany <http://www.cw.com/de/>
**
**  This file is part of OSSP lmtp2nntp, an LMTP speaking local
**  mailer which forwards mails as Usenet news articles via NNTP.
**  It can be found at http://www.ossp.org/pkg/tool/lmtp2nntp/.
**
**  This program is free software; you can redistribute it and/or
**  modify it under the terms of the GNU General Public  License
**  as published by the Free Software Foundation; either version
**  2.0 of the License, or (at your option) any later version.
**
**  This program is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
**  General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with this file; if not, write to the Free Software
**  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
**  USA, or contact the OSSP project <ossp@ossp.org>.
**
**  lmtp2nntp_shpat.c: Shell Pattern Matching library
*/

/*
 * Copyright (c) 1989, 1993, 1994
 *      The Regents of the University of California.  All rights reserved.
 *
 * This code is derived from software contributed to Berkeley by
 * Guido van Rossum.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *      This product includes software developed by the University of
 *      California, Berkeley and its contributors.
 * 4. Neither the name of the University nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

/*
 * Function shpat_match() as specified in POSIX 1003.2-1992, section B.6.
 * Compares a filename or pathname to a pattern.
 */

#include <ctype.h>
#include <string.h>
#include <stdio.h>

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif
#if defined(HAVE_DMALLOC_H) && defined(WITH_DMALLOC)
#include "dmalloc.h"
#endif

#include "lmtp2nntp_shpat.h"

#define EOS '\0'

#define RANGE_MATCH     1
#define RANGE_NOMATCH   0
#define RANGE_ERROR     (-1)

static int rangematch(const char *, char, int, char **);

int shpat_match(const char *pattern, const char *string, int flags)
{
    const char *stringstart;
    char *newp;
    char c, test;

    for (stringstart = string; ; ) {
        switch (c = *pattern++) {
            case EOS:
                if ((flags & SHPAT_M_LEADING_DIR) && *string == '/')
                    return (0);
                return (*string == EOS ? 0 : SHPAT_M_NOMATCH);
            case '?':
                if (*string == EOS)
                    return (SHPAT_M_NOMATCH);
                if (*string == '/' && (flags & SHPAT_M_PATHNAME))
                    return (SHPAT_M_NOMATCH);
                if (*string == '.' && (flags & SHPAT_M_PERIOD) &&
                    (string == stringstart ||
                     ((flags & SHPAT_M_PATHNAME) && *(string - 1) == '/')))
                    return (SHPAT_M_NOMATCH);
                ++string;
                break;
            case '*':
                c = *pattern;
                /* Collapse multiple stars. */
                while (c == '*')
                    c = *++pattern;

                if (*string == '.' && (flags & SHPAT_M_PERIOD) &&
                    (string == stringstart ||
                     ((flags & SHPAT_M_PATHNAME) && *(string - 1) == '/')))
                    return (SHPAT_M_NOMATCH);

                /* Optimize for pattern with * at end or before /. */
                if (c == EOS)
                    if (flags & SHPAT_M_PATHNAME)
                        return ((flags & SHPAT_M_LEADING_DIR) ||
                                strchr(string, '/') == NULL ?
                                0 : SHPAT_M_NOMATCH);
                    else
                        return (0);
                else if (c == '/' && flags & SHPAT_M_PATHNAME) {
                    if ((string = strchr(string, '/')) == NULL)
                        return (SHPAT_M_NOMATCH);
                    break;
                }

                /* General case, use recursion. */
                while ((test = *string) != EOS) {
                    if (!shpat_match(pattern, string, flags & ~SHPAT_M_PERIOD))
                        return (0);
                    if (test == '/' && flags & SHPAT_M_PATHNAME)
                        break;
                    ++string;
                }
                return (SHPAT_M_NOMATCH);
            case '[':
                if (*string == EOS)
                    return (SHPAT_M_NOMATCH);
                if (*string == '/' && (flags & SHPAT_M_PATHNAME))
                    return (SHPAT_M_NOMATCH);
                if (*string == '.' && (flags & SHPAT_M_PERIOD) &&
                    (string == stringstart ||
                     ((flags & SHPAT_M_PATHNAME) && *(string - 1) == '/')))
                    return (SHPAT_M_NOMATCH);

                switch (rangematch(pattern, *string, flags, &newp)) {
                    case RANGE_ERROR:
                        goto norm;
                    case RANGE_MATCH:
                        pattern = newp;
                        break;
                    case RANGE_NOMATCH:
                        return (SHPAT_M_NOMATCH);
                }
                ++string;
                break;
            case '\\':
                if (!(flags & SHPAT_M_NOESCAPE)) {
                    if ((c = *pattern++) == EOS) {
                        c = '\\';
                        --pattern;
                    }
                }
                /* FALLTHROUGH */
            default:
              norm:
                if (c == *string)
                    ;
                else if ((flags & SHPAT_M_CASEFOLD) &&
                         (tolower((unsigned char) c) ==
                          tolower((unsigned char) *string)))
                    ;
                else
                    return (SHPAT_M_NOMATCH);
                string++;
                break;
        }
    }
    /* NOTREACHED */
}

static int rangematch(
     const char *pattern,
     char test,
     int flags,
     char **newp)
{
    int negate, ok;
    char c, c2;

    /*
     * A bracket expression starting with an unquoted circumflex
     * character produces unspecified results (IEEE 1003.2-1992,
     * 3.13.2).  This implementation treats it like '!', for
     * consistency with the regular expression syntax.
     * J.T. Conklin (conklin@ngai.kaleida.com)
     */
    if ((negate = (*pattern == '!' || *pattern == '^')))
        ++pattern;

    if (flags & SHPAT_M_CASEFOLD)
        test = tolower((unsigned char) test);

    /*
     * A right bracket shall lose its special meaning and represent
     * itself in a bracket expression if it occurs first in the list.
     * -- POSIX.2 2.8.3.2
     */
    ok = 0;
    c = *pattern++;
    do {
        if (c == '\\' && !(flags & SHPAT_M_NOESCAPE))
            c = *pattern++;
        if (c == EOS)
            return (RANGE_ERROR);

        if (c == '/' && (flags & SHPAT_M_PATHNAME))
            return (RANGE_NOMATCH);

        if (flags & SHPAT_M_CASEFOLD)
            c = tolower((unsigned char) c);

        if (*pattern == '-'
            && (c2 = *(pattern + 1)) != EOS && c2 != ']') {
            pattern += 2;
            if (c2 == '\\' && !(flags & SHPAT_M_NOESCAPE))
                c2 = *pattern++;
            if (c2 == EOS)
                return (RANGE_ERROR);

            if (flags & SHPAT_M_CASEFOLD)
                c2 = tolower((unsigned char) c2);

            if (c <= test && test <= c2)
                ok = 1;
        }
        else if (c == test)
            ok = 1;
    } while ((c = *pattern++) != ']');

    *newp = (char *) pattern;
    return (ok == negate ? RANGE_NOMATCH : RANGE_MATCH);
}

/*
 * This function returns non-zero if pattern
 * contains any glob chars. Borrowed from Apache.
 */
int shpat_match_hasglobchar(const char *pattern)
{
    int nesting;

    nesting = 0;
    while (*pattern) {
        switch (*pattern) {
            case '?':
            case '*':
                return 1;
            case '\\':
                if (*pattern++ == EOS)
                    return 0;
                break;
            case '[':
                /* '[' is only a glob if it has a matching ']' */
                nesting++;
                break;
            case ']':
                if (nesting)
                    return 1;
                break;
        }
        pattern++;
    }
    return 0;
}

