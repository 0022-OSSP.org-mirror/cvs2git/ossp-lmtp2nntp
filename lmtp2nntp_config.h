/*
**  OSSP lmtp2nntp - Mail to News Gateway
**  Copyright (c) 2002-2003 Ralf S. Engelschall <rse@engelschall.com>
**  Copyright (c) 2002-2003 The OSSP Project <http://www.ossp.org/>
**  Copyright (c) 2002-2003 Cable & Wireless Germany <http://www.cw.com/de/>
**
**  This file is part of OSSP lmtp2nntp, an LMTP speaking local
**  mailer which forwards mails as Usenet news articles via NNTP.
**  It can be found at http://www.ossp.org/pkg/tool/lmtp2nntp/.
**
**  This program is free software; you can redistribute it and/or
**  modify it under the terms of the GNU General Public  License
**  as published by the Free Software Foundation; either version
**  2.0 of the License, or (at your option) any later version.
**
**  This program is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
**  General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with this file; if not, write to the Free Software
**  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
**  USA, or contact the OSSP project <ossp@ossp.org>.
**
**  lmtp2nntp_config.h: config handling
*/

#ifndef __LMTP2NNTP_CONFIG_H__
#define __LMTP2NNTP_CONFIG_H__

#include "lmtp2nntp_global.h"
#include "lmtp2nntp_common.h"

typedef enum {
    CONFIG_OK,
    CONFIG_OK_DRY,  /* dry run short circuit i.e. --testfile or --version */
    CONFIG_ERR_TRY, /* caught an exception from a underlying codeblock */
    CONFIG_ERR_LOG  /* cannot initialize logging */
} lmtp2nntp_config_rc_t;

lmtp2nntp_config_rc_t config_context(lmtp2nntp_t *);

#endif /* __LMTP2NNTP_CONFIG_H__ */

