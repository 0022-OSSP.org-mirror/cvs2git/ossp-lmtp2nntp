/*
**  OSSP lmtp2nntp - Mail to News Gateway
**  Copyright (c) 2001-2003 Ralf S. Engelschall <rse@engelschall.com>
**  Copyright (c) 2001-2003 The OSSP Project <http://www.ossp.org/>
**  Copyright (c) 2001-2003 Cable & Wireless Germany <http://www.cw.com/de/>
**
**  This file is part of OSSP lmtp2nntp, an LMTP speaking local
**  mailer which forwards mails as Usenet news articles via NNTP.
**  It can be found at http://www.ossp.org/pkg/tool/lmtp2nntp/.
**
**  This program is free software; you can redistribute it and/or
**  modify it under the terms of the GNU General Public  License
**  as published by the Free Software Foundation; either version
**  2.0 of the License, or (at your option) any later version.
**
**  This program is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
**  General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with this file; if not, write to the Free Software
**  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
**  USA, or contact the OSSP project <ossp@ossp.org>.
**
**  lmtp2nntp_msg.h: mail message manipulation library (API)
*/

#ifndef __MSG_H__
#define __MSG_H__

#include <sys/types.h>

#include "l2.h"
#include "val.h"
#include "lmtp2nntp_global.h"

extern void logbook(l2_channel_t *, l2_level_t, const char *, ...);

#include <pcre.h>
struct headerrule_st;
typedef struct headerrule_st headerrule_t;
struct headerrule_st {
    headerrule_t *next;
    int           pri;
    char         *regex;
    char         *name;
    char         *val;
    pcre         *pcreRegex;
    pcre_extra   *pcreExtra;
};

struct headerdata_st;
typedef struct headerdata_st headerdata_t;
struct headerdata_st {
    headerdata_t *prev;
    headerdata_t *next;
    char *name;
    int ndata; /* =0 means data is invalid, =1 means use data.s, >1 means use data.m */
    union {
        char *s;
        char **m;
    } data;
};

typedef struct {
    char   *azEnvgroups;  /* Groups according to Envelope in GROUPMODE_ENVELOPE */
    size_t  asEnvgroups;
    char   *cpMsg;        /* the wholly message to be received by DATA command */
    char   *azHeaders;    /* header part of message above */
    size_t  asHeaders;
    headerdata_t *hdFirst;
    char   *cpFid;        /* foreign (aka sendmail queue) id from parsing headers */
    char   *cpBody;       /* body part of message above */
    char   *cpMsgid;
    char   *mail_from;
    char   *azRcpt;
    size_t  asRcpt;
    char   *azNewsgroups;
    size_t  asNewsgroups;
    l2_channel_t *l2;     /* logging context, inherited as a copy from parent */
    val_t  *prival;       /* access to private application context */
} msg_t;

struct regex_ctx_st;
typedef struct regex_ctx_st regex_ctx_t;
struct regex_ctx_st {
    int            nMatch;
    const char   **acpMatch;
    l2_env_t      *l2_env;
    l2_channel_t  *l2;
};

#define WRAPAT 120          /* join wraps header lines when exceeding this value */
#define WRAPUSING "\n    "  /* join inserts this value when wrapping lines       */

#define DATELENMAX 32       /* strlen("Mon, 31 Jul 2002 12:34:56 +0123")+1 */

typedef enum {
    MSG_OK,
    MSG_ERR_MEM,
    MSG_ERR_SPLITHEADBODY,
    MSG_ERR_SPLITLEN,
    MSG_ERR_SPLITMISSINGFROM,
    MSG_ERR_JOINGROUPNONE,
    MSG_ERR_JOINGROUPEMPTY,
    MSG_ERR_JOINIDNONE,
    MSG_ERR_JOINIDEMPTY,
    MSG_ERR_JOINIDMULTI,
    MSG_ERR_ARG
} msg_rc_t;

msg_t    *msg_create(val_t *);
msg_rc_t  msg_split(msg_t *);
msg_rc_t  msg_join(msg_t *);
void      msg_destroy(msg_t *);
char     *msg_error(msg_rc_t);

#endif /*  __MSG_H__ */
