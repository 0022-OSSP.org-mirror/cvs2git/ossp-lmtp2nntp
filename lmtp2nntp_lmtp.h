/*
**  OSSP lmtp2nntp - Mail to News Gateway
**  Copyright (c) 2001-2003 Ralf S. Engelschall <rse@engelschall.com>
**  Copyright (c) 2001-2003 The OSSP Project <http://www.ossp.org/>
**  Copyright (c) 2001-2003 Cable & Wireless Germany <http://www.cw.com/de/>
**
**  This file is part of OSSP lmtp2nntp, an LMTP speaking local
**  mailer which forwards mails as Usenet news articles via NNTP.
**  It can be found at http://www.ossp.org/pkg/tool/lmtp2nntp/.
**
**  This program is free software; you can redistribute it and/or
**  modify it under the terms of the GNU General Public  License
**  as published by the Free Software Foundation; either version
**  2.0 of the License, or (at your option) any later version.
**
**  This program is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
**  General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with this file; if not, write to the Free Software
**  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
**  USA, or contact the OSSP project <ossp@ossp.org>.
**
**  lmtp2nntp_lmtp.h: Local Mail Transfer Protocol (LMTP) server library (API)
*/

#ifndef __LMTP_H__
#define __LMTP_H__

#include <sys/types.h>
#include <sys/uio.h>
#include <unistd.h>
#include <fcntl.h>

struct lmtp_st;
typedef struct lmtp_st lmtp_t;

typedef struct {
    void    *ctx;
    int     (*select)(void *, fd_set *, fd_set *, fd_set *, struct timeval *);
    ssize_t (*read)(void *, void *, size_t);
    ssize_t (*write)(void *, const void *, size_t);
} lmtp_io_t;

typedef struct {
    char *verb;
    char *msg;
} lmtp_req_t;

typedef struct {
    char *statuscode;
    char *dsncode;
    char *statusmsg;
} lmtp_res_t;

typedef enum {
    LMTP_OK,
    LMTP_EOF,
    LMTP_ERR_SYSTEM,
    LMTP_ERR_MEM,
    LMTP_ERR_OVERFLOW,
    LMTP_ERR_ARG,
    LMTP_ERR_UNKNOWN
} lmtp_rc_t;

typedef struct {
    int fd;
} lmtp_fd_t;

typedef lmtp_rc_t (*lmtp_cb_t)(lmtp_t *, lmtp_io_t *, lmtp_req_t *, void *);

lmtp_t     *lmtp_create  (lmtp_io_t *);
void        lmtp_destroy (lmtp_t *);
lmtp_rc_t   lmtp_readline(lmtp_t *, char *, size_t);
lmtp_rc_t   lmtp_readmsg (lmtp_t *, char **, size_t);
lmtp_rc_t   lmtp_request (lmtp_t *, lmtp_req_t *);
lmtp_rc_t   lmtp_response(lmtp_t *, lmtp_res_t *);
char       *lmtp_error   (lmtp_rc_t);
lmtp_rc_t   lmtp_register(lmtp_t *, char *, lmtp_cb_t, void *, lmtp_cb_t *, void **);
lmtp_rc_t   lmtp_loop    (lmtp_t *);
ssize_t     lmtp_fd_read (void *, void *, size_t);
ssize_t     lmtp_fd_write(void *, const void *, size_t);

#endif /* __LMTP_H__ */

