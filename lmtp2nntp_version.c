/*
**  lmtp2nntp_version.c -- Version Information for OSSP lmtp2nntp (syntax: C/C++)
**  [automatically generated and maintained by GNU shtool]
*/

#ifdef _LMTP2NNTP_VERSION_C_AS_HEADER_

#ifndef _LMTP2NNTP_VERSION_C_
#define _LMTP2NNTP_VERSION_C_

#define LMTP2NNTP_VERSION 0x104201

typedef struct {
    const int   v_hex;
    const char *v_short;
    const char *v_long;
    const char *v_tex;
    const char *v_gnu;
    const char *v_web;
    const char *v_sccs;
    const char *v_rcs;
} lmtp2nntp_version_t;

extern lmtp2nntp_version_t lmtp2nntp_version;

#endif /* _LMTP2NNTP_VERSION_C_ */

#else /* _LMTP2NNTP_VERSION_C_AS_HEADER_ */

#define _LMTP2NNTP_VERSION_C_AS_HEADER_
#include "lmtp2nntp_version.c"
#undef  _LMTP2NNTP_VERSION_C_AS_HEADER_

lmtp2nntp_version_t lmtp2nntp_version = {
    0x104201,
    "1.4.1",
    "1.4.1 (09-Oct-2005)",
    "This is OSSP lmtp2nntp, Version 1.4.1 (09-Oct-2005)",
    "OSSP lmtp2nntp 1.4.1 (09-Oct-2005)",
    "OSSP lmtp2nntp/1.4.1",
    "@(#)OSSP lmtp2nntp 1.4.1 (09-Oct-2005)",
    "$Id: OSSP lmtp2nntp 1.4.1 (09-Oct-2005) $"
};

#endif /* _LMTP2NNTP_VERSION_C_AS_HEADER_ */

