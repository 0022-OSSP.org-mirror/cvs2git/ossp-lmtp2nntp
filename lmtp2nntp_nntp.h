/*
**  OSSP lmtp2nntp - Mail to News Gateway
**  Copyright (c) 2001-2003 Ralf S. Engelschall <rse@engelschall.com>
**  Copyright (c) 2001-2003 The OSSP Project <http://www.ossp.org/>
**  Copyright (c) 2001-2003 Cable & Wireless Germany <http://www.cw.com/de/>
**
**  This file is part of OSSP lmtp2nntp, an LMTP speaking local
**  mailer which forwards mails as Usenet news articles via NNTP.
**  It can be found at http://www.ossp.org/pkg/tool/lmtp2nntp/.
**
**  This program is free software; you can redistribute it and/or
**  modify it under the terms of the GNU General Public  License
**  as published by the Free Software Foundation; either version
**  2.0 of the License, or (at your option) any later version.
**
**  This program is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
**  General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with this file; if not, write to the Free Software
**  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
**  USA, or contact the OSSP project <ossp@ossp.org>.
**
**  lmtp2nntp_nntp.h: Network News Transfer Protocol (NNTP) client library (API)
*/

#ifndef __NNTP_H__
#define __NNTP_H__

#include <sys/types.h>
#include <sys/uio.h>
#include <unistd.h>
#include <fcntl.h>

typedef enum {
    NNTP_OK,
    NNTP_EOF,
    NNTP_DEFER,
    NNTP_FAKE,
    NNTP_ERR_SYSTEM,
    NNTP_ERR_ARG,
    NNTP_ERR_OVERFLOW,
    NNTP_ERR_DELIVERY,
    NNTP_ERR_INIT,
    NNTP_ERR_UNKNOWN
} nntp_rc_t;

struct nntp_st;
typedef struct nntp_st nntp_t;
#include "lmtp2nntp_msg.h"

typedef struct {
    void    *ctx;
    ssize_t (*read)(void *, void *, size_t);
    ssize_t (*write)(void *, const void *, size_t);
} nntp_io_t;

typedef struct {
    int fd;
} nntp_fd_t;

nntp_t     *nntp_create   (nntp_io_t *);
nntp_rc_t   nntp_init     (nntp_t *);
void        nntp_destroy  (nntp_t *);
nntp_rc_t   nntp_readline (nntp_t *, char *, size_t);
nntp_rc_t   nntp_writeline(nntp_t *, char *);
nntp_rc_t   nntp_post     (nntp_t *, msg_t *msg);
nntp_rc_t   nntp_feed     (nntp_t *, msg_t *msg);
char       *nntp_lastresp (nntp_t *nntp);
char       *nntp_error    (nntp_rc_t);
ssize_t     nntp_fd_read  (void *, void *, size_t);
ssize_t     nntp_fd_write (void *, const void *, size_t);

#endif /* __NNTP_H__ */

