#/bin/sh

echon () {
    echo "$*" | awk '{ printf("%s", $0); }'
}

newmsg () {
    cat >${TMPFILE} <<EOT
LHLO dev.de.cw.net
MAIL From:<thomas.lotterer@example.com>
RCPT To:<quux.test@news-posting>
RCPT To:<foo.bar@news-posting>
RCPT To:<foo.test.bar@news-posting>
DATA
Date: Tue, 26 Aug 2001 14:51:48 +0200 (CEST)
Message-Id: <200108141251.f7ECpmn74812@dev.de.cw.net>
From: Thomas Lotterer <Thomas.Lotterer@example.com>
Received: from cw.example.com (cw.example.com [10.1.1.32])
    by gateway.example.com (8.9.2/8.9.2/$Revision: 1.17 $) with SMTP id NAA08840
    for <thomas.lotterer@gateway.example.com>; Mon, 2 Apr 2001 13:20:25 +0200 (MET DST)
Received: from history (history.example.org [195.143.102.41])
    by cw.example.com (8.11.0/8.11.0/$Revision: 1.17 $) with ESMTP id f32BKPb12235
    for <thomas.lotterer@example.com>; Mon, 2 Apr 2001 13:20:25 +0200
Received: from example.org (littlemua.example.org [195.143.103.160])
	by history (8.8.8/8.8.8) with ESMTP id LAA12678;
	Mon, 2 Apr 2001 11:23:14 GMT
X-Linefoldingtest: This is a very long line. It is assumed this line will be
    wrapped in order to fit. Please note: the quick brown fox jumps over the lazy
    dog. Did you know that my first computer was a Texas Instruments TI99/4A?
    What a device! Had a 16bit CPU before the Commodore C64 area has begun.
Subject: lmtp2nntp testmessage.viasendmail

..
a dot above
#
a dot below
..
.
QUIT
EOT
    awk <${TMPFILE} \
     '  BEGIN          { gotit = 0; IGNORECASE = 1; }
        /^#$/          { gotit = 1; print "#" date " [" pid "] " id; }
        /^Message-ID:/ { gotit = 1; print "Message-Id: <" date "-" id "@example.com>"; }
        /^Subject:/    { gotit = 1; print "Subject: [" pid "] " $2; }
                       { if (gotit == 0) { print; }; gotit = 0; }
     ' pid="$$" id="I${H}${Z}${E}D" date="`date +%Y%m%d%H%M%S`" \
     | tee 2>&1 >/dev/null ${STDIN}
}

prolog () {
    H="0"; Z="0"; E="0"; #HZE used for mass-tests with uniq IDs

    for i in ${FILE} ${STDIN} ${STDOUT} ${STDERR} ${DMALLOC}; do
        if [ -f ${i} ]; then
            rm -f ${i}
        fi
        touch ${i}
        chmod 666 ${i}
    done
}

epilog () {
    for i in ${FILE} ${STDIN} ${STDOUT} ${STDERR} ${DMALLOC}; do
        if [ -r ${i} ]; then
            echon "press RETURN to see the contents of ${i}"
            read DUMMY
            less -S ${i}
        fi
    done
}

#main ()

 TMPDIR="${TMPDIR:-/tmp}"
 PREFIX="${TMPDIR}/lmtp2nntp.test."
TMPFILE="${PREFIX}tmp"
DMALLOC="${PREFIX}dmalloc.log"

 L2CONF="${PREFIX}logspec"
 L2FILE="${PREFIX}log"
 L2SPEC="'debug: prefix(prefix=\"%b %d %H:%M:%S <%L> lmtp2nntp[%P]: \",timezone=local) -> file(path=\"${L2FILE}\",append=0,perm=432)'"
 echo >${L2CONF} "l2spec ${L2SPEC}"
    LOG="-i ${L2CONF}"

  STDIN="${PREFIX}stdin"
 STDOUT="${PREFIX}stdout"
 STDERR="${PREFIX}stderr"
   HOST="0.0.0.0"
  GROUP="my.test"
  MFILT=".+@(?:[^.]+\.)*example\.com"
   NODE="gateway"

# make check with automatic succeed/ fail message.
# more samples and testing below.

echon "checking whether -v version option works ... "
# expected sample output: lmtp2nntp 0.9.4 (30-Aug-2001)
prolog
newmsg
./lmtp2nntp -v >${STDOUT} 2>${STDERR}
RC=`cat ${STDOUT} | egrep 'lmtp2nntp[ ]+[0-9]+\.[0-9]+[ab\.][0-9]+[ ]+\([0-9]+-(Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec)-[0-9]+\)' | wc -l`
if [ ${RC} -ne 1 -o ".`cat ${STDERR}`" != . ]; then
    echo "NO (got ${RC})"
    echo "STDIN  cat ${STDIN}"
    echo "STDOUT cat ${STDOUT}"
    echo "STDERR cat ${STDERR}"
    echo "LOG    cat ${L2FILE}"
    exit 1;
fi
echo "yes"

echon "checking whether -? usage option works ... "
# expected typical output see ./lmtp2nntp -?
prolog
newmsg
./lmtp2nntp -? >${STDOUT} 2>${STDERR}
RC="";
if [ 1 -eq `egrep <${STDOUT} -- '-C.*--childsmax'       | wc -l` ]; then RC="${RC}-C "; fi
if [ 1 -eq `egrep <${STDOUT} -- '-D.*--daemonize'       | wc -l` ]; then RC="${RC}-D "; fi
if [ 1 -eq `egrep <${STDOUT} -- '-K.*--kill'            | wc -l` ]; then RC="${RC}-K "; fi
if [ 1 -eq `egrep <${STDOUT} -- '-P.*--pidfile'         | wc -l` ]; then RC="${RC}-P "; fi
if [ 1 -eq `egrep <${STDOUT} -- '-a.*--acl'             | wc -l` ]; then RC="${RC}-a "; fi
if [ 1 -eq `egrep <${STDOUT} -- '-b.*--bind'            | wc -l` ]; then RC="${RC}-b "; fi
if [ 1 -eq `egrep <${STDOUT} -- '-c.*--client'          | wc -l` ]; then RC="${RC}-c "; fi
if [ 1 -eq `egrep <${STDOUT} -- '-d.*--destination'     | wc -l` ]; then RC="${RC}-d "; fi
if [ 1 -eq `egrep <${STDOUT} -- '-g.*--groupmode'       | wc -l` ]; then RC="${RC}-g "; fi
if [ 1 -eq `egrep <${STDOUT} -- '-h.*--headerrule'      | wc -l` ]; then RC="${RC}-h "; fi
if [ 1 -eq `egrep <${STDOUT} -- '-i.*--include'         | wc -l` ]; then RC="${RC}-i "; fi
if [ 1 -eq `egrep <${STDOUT} -- '--timeoutlmtp'         | wc -l` ]; then RC="${RC}--timeoutlmtp "; fi
if [ 1 -eq `egrep <${STDOUT} -- '--timeoutlmtpaccept'   | wc -l` ]; then RC="${RC}--timeoutlmtpaccept "; fi
if [ 1 -eq `egrep <${STDOUT} -- '--timeoutlmtpread'     | wc -l` ]; then RC="${RC}--timeoutlmtpread "; fi
if [ 1 -eq `egrep <${STDOUT} -- '--timeoutlmtpwrite'    | wc -l` ]; then RC="${RC}--timeoutlmtpwrite "; fi
if [ 1 -eq `egrep <${STDOUT} -- '--timeoutnntp'         | wc -l` ]; then RC="${RC}--timeoutnntp "; fi
if [ 1 -eq `egrep <${STDOUT} -- '--timeoutnntpconnect'  | wc -l` ]; then RC="${RC}--timeoutnntpconnect "; fi
if [ 1 -eq `egrep <${STDOUT} -- '--timeoutnntpread'     | wc -l` ]; then RC="${RC}--timeoutnntpread "; fi
if [ 1 -eq `egrep <${STDOUT} -- '--timeoutnntpwrite'    | wc -l` ]; then RC="${RC}--timeoutnntpwrite "; fi
if [ 1 -eq `egrep <${STDOUT} -- '-l.*--l2spec'          | wc -l` ]; then RC="${RC}-l "; fi
if [ 1 -eq `egrep <${STDOUT} -- '-m.*--mailfrom'        | wc -l` ]; then RC="${RC}-m "; fi
if [ 1 -eq `egrep <${STDOUT} -- '-n.*--nodename'        | wc -l` ]; then RC="${RC}-n "; fi
if [ 1 -eq `egrep <${STDOUT} -- '-o.*--operationmode'   | wc -l` ]; then RC="${RC}-o "; fi
if [ 1 -eq `egrep <${STDOUT} -- '-r.*--restrictheader'  | wc -l` ]; then RC="${RC}-r "; fi
if [ 1 -eq `egrep <${STDOUT} -- '-s.*--size'            | wc -l` ]; then RC="${RC}-s "; fi
if [ 1 -eq `egrep <${STDOUT} -- '-u.*--user'            | wc -l` ]; then RC="${RC}-u "; fi
if [ 1 -eq `egrep <${STDOUT} -- '-v.*--version'         | wc -l` ]; then RC="${RC}-v "; fi
if [ 1 -eq `egrep <${STDOUT} -- '--newsgroup'           | wc -l` ]; then RC="${RC}--newsgroup "; fi
if [ 1 -eq `egrep <${STDOUT} -- '-?.*--help'            | wc -l` ]; then RC="${RC}-? "; fi
if [ 1 -eq `egrep <${STDOUT} -- '--usage'               | wc -l` ]; then RC="${RC}--usage "; fi
if [ "${RC}" != "-C -D -K -P -a -b -c -d -g -h -i --timeoutlmtpaccept --timeoutlmtpread --timeoutlmtpwrite --timeoutnntpconnect --timeoutnntpread --timeoutnntpwrite -l -m -n -o -r -s -u -v --newsgroup -? --usage " ]; then
    echo "NO (got ${RC})"
    echo "STDIN  cat ${STDIN}"
    echo "STDOUT cat ${STDOUT}"
    echo "STDERR cat ${STDERR}"
    echo "LOG    cat ${L2FILE}"
    exit 1;
fi
echo "yes"

echon "checking whether a valid fake posting succeeds ... "
prolog
newmsg
./lmtp2nntp <${STDIN} -b - -o 250/2.0.0 -g arg -d ${HOST} ${LOG} -m "${MFILT}" -n ${NODE} -s 1500 --timeoutnntpconnect=1 ${GROUP} >${STDOUT} 2>${STDERR} ; RC=$
RC="";
if [ 1 -eq `egrep <${STDOUT} '^220[- ]LMTP Service ready'       | wc -l` ]; then RC="${RC}init "; fi
if [ 1 -eq `egrep <${STDOUT} '^250[- ].*pleased to meet you'    | wc -l` ]; then RC="${RC}LHLO "; fi
if [ 1 -eq `egrep <${STDOUT} '^250[- ]2.1.0 Sender ok'          | wc -l` ]; then RC="${RC}MAIL "; fi
if [ 3 -eq `egrep <${STDOUT} '^250[- ]2.1.5 Recipient accepted' | wc -l` ]; then RC="${RC}RCPT "; fi
if [ 1 -eq `egrep <${STDOUT} '^354[- ]Enter mail'               | wc -l` ]; then RC="${RC}DATA "; fi
if [ 3 -eq `egrep <${STDOUT} '^250[- ]2.0.0 NNTP noop fake'     | wc -l` ]; then RC="${RC}post "; fi
if [ 1 -eq `egrep <${STDOUT} '^221[- ]2.0.0.+closing.+channel'  | wc -l` ]; then RC="${RC}QUIT "; fi
if [ "${RC}" != "init LHLO MAIL RCPT DATA post QUIT " ]; then
    echo "NO (got ${RC})"
    echo "STDIN  cat ${STDIN}"
    echo "STDOUT cat ${STDOUT}"
    echo "STDERR cat ${STDERR}"
    echo "LOG    cat ${L2FILE}"
    exit 1;
fi
echo "yes"

echon "checking whether -m option blocks invalid sender ... "
prolog
newmsg
( MFILT=".*@is.invalid" ; ./lmtp2nntp <${STDIN} -b - -o 250/2.0.0 -g arg -d ${HOST} ${LOG} -m "${MFILT}" -n ${NODE} -s 1500 --timeoutnntpconnect=1 ${GROUP} >${STDOUT} 2>${STDERR} ) ; RC=$?
RC="";
if [ 1 -eq `egrep <${STDOUT} '^220[- ]LMTP Service ready'       | wc -l` ]; then RC="${RC}init "; fi
if [ 1 -eq `egrep <${STDOUT} '^250[- ].+pleased to meet you'    | wc -l` ]; then RC="${RC}LHLO "; fi
if [ 1 -eq `egrep <${STDOUT} '^550[- ]5.7.1.+not authorized'    | wc -l` ]; then RC="${RC}MAIL "; fi
if [ 3 -eq `egrep <${STDOUT} '^503[- ]5.5.0.+MAIL first'        | wc -l` ]; then RC="${RC}RCPT "; fi
if [ 1 -eq `egrep <${STDOUT} '^503[- ]5.5.0.+RCPT first'        | wc -l` ]; then RC="${RC}DATA "; fi
if [ 0 -lt `egrep <${STDOUT} '^500[- ]5.5.1.+unrecognized'      | wc -l` ]; then RC="${RC}post "; fi
if [ 1 -eq `egrep <${STDOUT} '^221[- ]2.0.0.+closing.+channel'  | wc -l` ]; then RC="${RC}QUIT "; fi
if [ "${RC}" != "init LHLO MAIL RCPT DATA post QUIT " ]; then
    echo "NO (got ${RC})"
    echo "STDIN  cat ${STDIN}"
    echo "STDOUT cat ${STDOUT}"
    echo "STDERR cat ${STDERR}"
    echo "LOG    cat ${L2FILE}"
    exit 1;
fi
echo "yes"

echon "checking whether -s option rejects article with invalid size ... "
prolog
newmsg
./lmtp2nntp <${STDIN} -b - -o 250/2.0.0 -g arg -d ${HOST} ${LOG} -m "${MFILT}" -n ${NODE} -s 100 --timeoutnntpconnect=1 ${GROUP} >${STDOUT} 2>${STDERR} ; RC=$?
RC="";
if [ 1 -eq `egrep <${STDOUT} '^220[- ]LMTP Service ready'       | wc -l` ]; then RC="${RC}init "; fi
if [ 1 -eq `egrep <${STDOUT} '^250[- ].*pleased to meet you'    | wc -l` ]; then RC="${RC}LHLO "; fi
if [ 1 -eq `egrep <${STDOUT} '^250[- ]2.1.0 Sender ok'          | wc -l` ]; then RC="${RC}MAIL "; fi
if [ 3 -eq `egrep <${STDOUT} '^250[- ]2.1.5 Recipient accepted' | wc -l` ]; then RC="${RC}RCPT "; fi
if [ 1 -eq `egrep <${STDOUT} '^354[- ]Enter mail'               | wc -l` ]; then RC="${RC}DATA "; fi
if [ 3 -eq `egrep <${STDOUT} '^552[- ]5.2.3.+exceeds.+limit'    | wc -l` ]; then RC="${RC}post "; fi
if [ 1 -eq `egrep <${STDOUT} '^221[- ]2.0.0.+closing.+channel'  | wc -l` ]; then RC="${RC}QUIT "; fi
if [ "${RC}" != "init LHLO MAIL RCPT DATA post QUIT " ]; then
    echo "NO (got ${RC})"
    echo "STDIN  cat ${STDIN}"
    echo "STDOUT cat ${STDOUT}"
    echo "STDERR cat ${STDERR}"
    echo "LOG    cat ${L2FILE}"
    exit 1;
fi
echo "yes"

echon "checking whether -c option fails on invalid local host address ... "
prolog
newmsg
( LOCAL="10.255.255.255" ; ./lmtp2nntp <${STDIN} -b - -c ${LOCAL} -o 250/2.0.0 -g arg -d ${HOST} ${LOG} -m "${MFILT}" -n ${NODE} -s 1500 --timeoutnntpconnect=1 ${GROUP} >${STDOUT} 2>${STDERR} ) ; RC=$?
RC="";
if [ 1 -eq `egrep <${STDOUT} '^220[- ]LMTP Service ready'       | wc -l` ]; then RC="${RC}init "; fi
if [ 1 -eq `egrep <${L2FILE} 'error.+binding'                   | wc -l` ]; then RC="${RC}bind "; fi
if [ "${RC}" != "init bind " ]; then
    echo "NO (got ${RC})"
    echo "STDIN  cat ${STDIN}"
    echo "STDOUT cat ${STDOUT}"
    echo "STDERR cat ${STDERR}"
    echo "LOG    cat ${L2FILE}"
    exit 1;
fi
echo "yes"

echon "checking whether -g envelope option blocks invalid group ... "
prolog
newmsg
( GROUP="foo.*" ; ./lmtp2nntp <${STDIN} -b - -o 250/2.0.0 -g envelope -d ${HOST} ${LOG} -m "${MFILT}" -n ${NODE} -s 1500 --timeoutnntpconnect=1 ${GROUP} >${STDOUT} 2>${STDERR} ) ; RC=$?
RC="";
if [ 1 -eq `egrep <${STDOUT} '^220[- ]LMTP Service ready'       | wc -l` ]; then RC="${RC}init "; fi
if [ 1 -eq `egrep <${STDOUT} '^250[- ].*pleased to meet you'    | wc -l` ]; then RC="${RC}LHLO "; fi
if [ 1 -eq `egrep <${STDOUT} '^250[- ]2.1.0 Sender ok'          | wc -l` ]; then RC="${RC}MAIL "; fi
if [ 1 -eq `egrep <${STDOUT} '^550[- ]5.7.2 unmatched Group'    | wc -l` ]; then RC="${RC}RCPT "; fi
if [ 2 -eq `egrep <${STDOUT} '^250[- ]2.1.5 Group accepted'     | wc -l` ]; then RC="${RC}RCPT "; fi
if [ 1 -eq `egrep <${STDOUT} '^354[- ]Enter mail'               | wc -l` ]; then RC="${RC}DATA "; fi
if [ 2 -eq `egrep <${STDOUT} '^250[- ]2.0.0 NNTP noop fake'     | wc -l` ]; then RC="${RC}post "; fi
if [ 1 -eq `egrep <${STDOUT} '^221[- ]2.0.0.+closing.+channel'  | wc -l` ]; then RC="${RC}QUIT "; fi
if [ "${RC}" != "init LHLO MAIL RCPT RCPT DATA post QUIT " ]; then
    echo "NO (got ${RC})"
    echo "STDIN  cat ${STDIN}"
    echo "STDOUT cat ${STDOUT}"
    echo "STDERR cat ${STDERR}"
    echo "LOG    cat ${L2FILE}"
    exit 1;
fi
echo "yes"

echon "checking whether -d option times out for invalid host ... "
prolog
newmsg
( HOST="10.255.255.255" ; ./lmtp2nntp <${STDIN} -b - -o 250/2.0.0 -g arg -d ${HOST} ${LOG} -m "${MFILT}" -n ${NODE} -s 1500 --timeoutnntpconnect=1 ${GROUP} >${STDOUT} 2>${STDERR} ) ; RC=$?
RC="";
if [ 1 -eq `egrep <${STDOUT} '^220[- ]LMTP Service ready'       | wc -l` ]; then RC="${RC}init "; fi
if [ 1 -eq `egrep <${L2FILE} 'warning.+connect.+failed'         | wc -l` ]; then RC="${RC}conn "; fi
if [ "${RC}" != "init conn " ]; then
    echo "NO (got ${RC})"
    echo "STDIN  cat ${STDIN}"
    echo "STDOUT cat ${STDOUT}"
    echo "STDERR cat ${STDERR}"
    echo "LOG    cat ${L2FILE}"
    exit 1;
fi
echo "yes"

echon "checking whether -h option adds header/value pair ... "
prolog
newmsg
./lmtp2nntp <${STDIN} -b - -o 250/2.0.0 -g arg -d ${HOST} -h "::X-Gateway:added it!" ${LOG} -m "${MFILT}" -n ${NODE} -s 1500 --timeoutnntpconnect=1 ${GROUP} >${STDOUT} 2>${STDERR}
RC="";
if [ 1 -eq `egrep <${STDOUT} '^220[- ]LMTP Service ready'       | wc -l` ]; then RC="${RC}init "; fi
if [ 1 -eq `egrep <${L2FILE} 'appending header X-Gateway:'      | wc -l` ]; then RC="${RC}head "; fi
if [ 1 -eq `egrep <${L2FILE} 'header=X-Gateway.*data=added it!' | wc -l` ]; then RC="${RC}val. "; fi
if [ 1 -eq `egrep <${L2FILE} 'verbatim.*"X-Gateway: added it!"' | wc -l` ]; then RC="${RC}pair "; fi
if [ "${RC}" != "init head val. pair " ]; then
    echo "NO (got ${RC})"
    echo "STDIN  cat ${STDIN}"
    echo "STDOUT cat ${STDOUT}"
    echo "STDERR cat ${STDERR}"
    echo "LOG    cat ${L2FILE}"
    exit 1;
fi
echo "yes"

echon "checking whether -r option rejects headers ... "
prolog
newmsg
./lmtp2nntp <${STDIN} -b - -o 250/2.0.0 -g arg -d ${HOST} -r'^X-Linefoldingtest:.*begun.$' ${LOG} -m "${MFILT}" -n ${NODE} -s 1500 --timeoutnntpconnect=1 ${GROUP} >${STDOUT} 2>${STDERR}
RC="";
if [ 1 -eq `egrep <${STDOUT} '^220[- ]LMTP Service ready'       | wc -l` ]; then RC="${RC}init "; fi
if [ 1 -eq `egrep <${L2FILE} 'restricted header found'          | wc -l` ]; then RC="${RC}rest "; fi
if [ "${RC}" != "init rest " ]; then
    echo "NO (got ${RC})"
    echo "STDIN  cat ${STDIN}"
    echo "STDOUT cat ${STDOUT}"
    echo "STDERR cat ${STDERR}"
    echo "LOG    cat ${L2FILE}"
    exit 1;
fi
echo "yes"
echo "CHECK COMPLETE AND PASSED"
#epilog
exit 0
