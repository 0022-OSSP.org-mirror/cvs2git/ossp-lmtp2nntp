/*
**  OSSP lmtp2nntp - Mail to News Gateway
**  Copyright (c) 2002-2003 Ralf S. Engelschall <rse@engelschall.com>
**  Copyright (c) 2002-2003 The OSSP Project <http://www.ossp.org/>
**  Copyright (c) 2002-2003 Cable & Wireless Germany <http://www.cw.com/de/>
**
**  This file is part of OSSP lmtp2nntp, an LMTP speaking local
**  mailer which forwards mails as Usenet news articles via NNTP.
**  It can be found at http://www.ossp.org/pkg/tool/lmtp2nntp/.
**
**  This program is free software; you can redistribute it and/or
**  modify it under the terms of the GNU General Public  License
**  as published by the Free Software Foundation; either version
**  2.0 of the License, or (at your option) any later version.
**
**  This program is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
**  General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with this file; if not, write to the Free Software
**  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
**  USA, or contact the OSSP project <ossp@ossp.org>.
**
**  lmtp2nntp_option.h: option parsing
*/

#ifndef __LMTP2NNTP_OPTION_H__
#define __LMTP2NNTP_OPTION_H__

#include "lmtp2nntp_global.h"
#include "val.h"

struct optionval_s;
typedef struct optionval_s optionval_t;

typedef struct {
    optionval_t        *first;
    optionval_t        *last;
    val_t              *vo;     /* val_t for all options */
    int                 pi;     /* popt index to next record */
    int                 pn;     /* popt number of available records */
    struct popt_option *pt;     /* popt table */

} lmtp2nntp_option_t;

typedef enum {
    OPT_FLAG,
    OPT_SINGLE,
    OPT_MULTI
} optiontype_t;

typedef enum {
    OPTION_OK,
    OPTION_ERR_ARG, /* invalid args passed into function */
    OPTION_ERR_TRY, /* caught an exception from a underlying codeblock */
    OPTION_ERR_USE, /* invalid usage, bad data passed into function */
    OPTION_ERR_MEM, /* out of memory */
    OPTION_ERR_VAL, /* libval failed */
    OPTION_ERR_NUM  /* option_find not successful */
} lmtp2nntp_option_rc_t;

typedef lmtp2nntp_option_rc_t (optionloop_cb_t)(optionval_t *oc, char *arg, char *cbctx);

struct optionval_s {
    optionval_t        *next;           /* cleanup chain for destroy */
    lmtp2nntp_option_t *parent;         /* include needs access to parent */
                    /**/
    char               *longname;       /* the long name (optional if shortname given) */
    char                shortname;      /* the short name (optional if longname given) */
    char               *descrip;        /* description for autohelp */
    char               *argdescrip;     /* argument description for autohelp */
    optiontype_t        type;           /* OPT_FLAG, OPT_SINGLE, OPT_MULTI */
    optionloop_cb_t    *cb;             /* callback for first iteration - syntax check and include */
    void               *cbctx;          /* context for pass1 */
    val_t              *val;            /* val we are registered under */
                    /**/
    int                 number;         /* number of this option for popt */
    union {                             /* option data as read from configuration */
        int f;                          /* OPT_FLAG */
        char *s;                        /* OPT_SINGLE */
        char **m;                       /* OPT_MULTI */
    } data;
    int ndata;
};

/* these public functions catch all underlying exceptions and properly return a code */
lmtp2nntp_option_rc_t option_create (lmtp2nntp_option_t **, val_t *);
lmtp2nntp_option_rc_t option_parse  (lmtp2nntp_option_t *, int, char **);
lmtp2nntp_option_rc_t option_destroy(lmtp2nntp_option_t *);

#endif /* __LMTP2NNTP_OPTION_H__ */
