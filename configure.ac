dnl ##
dnl ##  OSSP lmtp2nntp - Mail to News Gateway
dnl ##  Copyright (c) 2001-2003 Ralf S. Engelschall <rse@engelschall.com>
dnl ##  Copyright (c) 2001-2003 The OSSP Project <http://www.ossp.org/>
dnl ##  Copyright (c) 2001-2003 Cable & Wireless Germany <http://www.cw.com/de/>
dnl ##
dnl ##  This file is part of OSSP lmtp2nntp, an LMTP speaking local
dnl ##  mailer which forwards mails as Usenet news articles via NNTP.
dnl ##  It can be found at http://www.ossp.org/pkg/tool/lmtp2nntp/.
dnl ##
dnl ##  This program is free software; you can redistribute it and/or
dnl ##  modify it under the terms of the GNU General Public  License
dnl ##  as published by the Free Software Foundation; either version
dnl ##  2.0 of the License, or (at your option) any later version.
dnl ##
dnl ##  This program is distributed in the hope that it will be useful,
dnl ##  but WITHOUT ANY WARRANTY; without even the implied warranty of
dnl ##  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
dnl ##  General Public License for more details.
dnl ##
dnl ##  You should have received a copy of the GNU General Public License
dnl ##  along with this file; if not, write to the Free Software
dnl ##  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
dnl ##  USA, or contact the OSSP project <ossp@ossp.org>.
dnl ##
dnl ##  configure.ac: GNU Autoconf script input (M4 syntax)
dnl ##

dnl #   standard preamble
AC_PREREQ(2.53)
AC_INIT

dnl #   announce our version
V=`./shtool version -lc -dlong lmtp2nntp_version.c`
./shtool echo -e "Configuring %BOSSP lmtp2nntp%b, Version %B${V}%b"
echo "Copyright (c) 2001-2003 Ralf S. Engelschall <rse@engelschall.com>"
echo "Copyright (c) 2001-2003 The OSSP Project <http://www.ossp.org/>"
echo "Copyright (c) 2001-2003 Cable & Wireless Germany <http://www.cw.com/de/>"

dnl #   perform feature tests
AC_PROG_MAKE_SET
AC_PROG_CC
AC_PROG_CPP
AC_CHECK_DEBUGGING
AC_CHECK_LIB(socket, getprotobyname)
AC_CHECK_LIB(nsl, gethostbyname)
AC_CHECK_FUNCS(inet_pton inet_aton)
AC_CHECK_FUNCS(setsid)
AC_CHECK_HEADERS(sys/termios.h)

dnl #   checks for POPT library
AC_CHECK_HEADERS(alloca.h float.h)

dnl #   check for OSSP ex library
AC_CHECK_EXTLIB([OSSP ex],
                ex, __ex_ctx, ex.h,
                [SUBDIR_EX=""],
                [SUBDIR_EX="lib_ex"
                 CPPFLAGS="-Ilib_ex $CPPFLAGS"
                 CFLAGS="-Ilib_ex $CFLAGS"
                 LDFLAGS="-Llib_ex/.libs $LDFLAGS"
                 LIBS_EXTRA="$LIBS_EXTRA -lex"])
AC_SUBST(SUBDIR_EX)

dnl #   check for OSSP str library
AC_CHECK_EXTLIB([OSSP str],
                str, str_parse, str.h,
                [SUBDIR_STR=""],
                [SUBDIR_STR="lib_str"
                 CPPFLAGS="-Ilib_str $CPPFLAGS"
                 CFLAGS="-Ilib_str $CFLAGS"
                 LDFLAGS="-Llib_str/.libs $LDFLAGS"
                 LIBS_EXTRA="$LIBS_EXTRA -lstr"])
AC_SUBST(SUBDIR_STR)

dnl #   check for OSSP l2 library
AC_CHECK_EXTLIB([OSSP l2],
                l2, l2_stream_log, l2.h,
                [SUBDIR_L2=""],
                [SUBDIR_L2="lib_l2"
                 CPPFLAGS="-Ilib_l2 $CPPFLAGS"
                 CFLAGS="-Ilib_l2 $CFLAGS"
                 LDFLAGS="-Llib_l2/.libs $LDFLAGS"
                 LIBS_EXTRA="$LIBS_EXTRA -ll2"])
AC_SUBST(SUBDIR_L2)

dnl #   check for OSSP sa library
AC_CHECK_EXTLIB([OSSP sa],
                sa, sa_create, sa.h,
                [SUBDIR_SA=""],
                [SUBDIR_SA="lib_sa"
                 CPPFLAGS="-Ilib_sa $CPPFLAGS"
                 CFLAGS="-Ilib_sa $CFLAGS"
                 LDFLAGS="-Llib_sa/.libs $LDFLAGS"
                 LIBS_EXTRA="$LIBS_EXTRA -lsa"])
AC_SUBST(SUBDIR_SA)

dnl #   check for OSSP var library
AC_CHECK_EXTLIB([OSSP var],
                var, var_expand, var.h,
                [SUBDIR_VAR=""],
                [SUBDIR_VAR="lib_var"
                 CPPFLAGS="-Ilib_var $CPPFLAGS"
                 CFLAGS="-Ilib_var $CFLAGS"
                 LDFLAGS="-Llib_var/.libs $LDFLAGS"
                 LIBS_EXTRA="$LIBS_EXTRA -lvar"])
AC_SUBST(SUBDIR_VAR)

dnl #   check for OSSP val library
AC_CHECK_EXTLIB([OSSP val],
                val, val_create, val.h,
                [SUBDIR_VAL=""],
                [SUBDIR_VAL="lib_val"
                 CPPFLAGS="-Ilib_val $CPPFLAGS"
                 CFLAGS="-Ilib_val $CFLAGS"
                 LDFLAGS="-Llib_val/.libs $LDFLAGS"
                 LIBS_EXTRA="$LIBS_EXTRA -lval"])
AC_SUBST(SUBDIR_VAL)

dnl #   check for OSSP popt library
AC_CHECK_EXTLIB([OSSP popt],
                popt, popt_getcontext, popt.h,
                [SUBDIR_POPT=""],
                [SUBDIR_POPT="lib_popt"
                 CPPFLAGS="-Ilib_popt $CPPFLAGS"
                 CFLAGS="-Ilib_popt $CFLAGS"
                 LDFLAGS="-Llib_popt/.libs $LDFLAGS"
                 LIBS_EXTRA="$LIBS_EXTRA -lpopt"])
AC_SUBST(SUBDIR_POPT)

dnl #   check for OSSP pcre library
AC_CHECK_EXTLIB([OSSP pcre],
                pcre, pcre_compile, pcre.h,
                [SUBDIR_PCRE=""],
                [SUBDIR_PCRE="lib_pcre"
                 CPPFLAGS="-Ilib_pcre $CPPFLAGS"
                 CFLAGS="-Ilib_pcre $CFLAGS"
                 LDFLAGS="-Llib_pcre/.libs $LDFLAGS"
                 LIBS_EXTRA="$LIBS_EXTRA -lpcre"])
AC_SUBST(SUBDIR_PCRE)

dnl #   check for external Dmalloc library
AC_CHECK_EXTLIB([Dmalloc], dmalloc, dmalloc_debug, dmalloc.h,
                [AC_DEFINE(WITH_DMALLOC, 1, [Define to 1 if building with Dmalloc])])

dnl #   post-processing for LIBS_EXTRA
LIBS="$LIBS_EXTRA $LIBS"

enable_shared=no
export enable_shared
enable_headline=no
export enable_headline
if test ".$SUBDIR_EX"   != .; then AC_CONFIG_SUBDIRS(lib_ex)   fi
if test ".$SUBDIR_STR"  != .; then AC_CONFIG_SUBDIRS(lib_str)  fi
if test ".$SUBDIR_L2"   != .; then AC_CONFIG_SUBDIRS(lib_l2)   fi
if test ".$SUBDIR_SA"   != .; then AC_CONFIG_SUBDIRS(lib_sa)   fi
if test ".$SUBDIR_VAR"  != .; then AC_CONFIG_SUBDIRS(lib_var)  fi
if test ".$SUBDIR_VAL"  != .; then AC_CONFIG_SUBDIRS(lib_val)  fi
if test ".$SUBDIR_POPT" != .; then AC_CONFIG_SUBDIRS(lib_popt) fi
if test ".$SUBDIR_PCRE" != .; then AC_CONFIG_SUBDIRS(lib_pcre) fi

dnl #   standard output generation
AC_CONFIG_HEADERS(config.h)
AC_CONFIG_FILES(Makefile)
AC_OUTPUT

